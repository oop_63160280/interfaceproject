/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.interfaceproject;

/**
 *
 * @author User
 */
public class Dog extends LandAnimal{
    
    private String nickname;

    public Dog(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Dog: " + nickname + " like to eat everything ");
    }

    @Override
    public void walk() {
        System.out.println("Dog: " + nickname + " walk ");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: " + nickname + " like to sleep in bed ");
    }

    @Override
    public void speak() {
        System.out.println("Dog: " + nickname + " bark bockbock ");
    }

    @Override
    public void run() {
        System.out.println("Dog: " + nickname + " run ");
    }
    
}
