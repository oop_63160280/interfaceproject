/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.interfaceproject;

/**
 *
 * @author User
 */
public class Bat extends Poultry{
    
    private String nickname;

    public Bat(String nickname) {
        super();
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickname + " eat insects");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickname + " not walking");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickname + " sleep with head hanging");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickname + " speak");
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + nickname + " fly");
    }

    
    
}
