/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.interfaceproject;

/**
 *
 * @author User
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("wiss");
        Plane plane = new Plane("Engine number 1");
        bat.fly();  //Animal,Poultry,Flyable
        plane.fly();    //Vahicle,Flyable
        System.out.println("------------");
     
        Dog dog = new Dog("noh");
        dog.run();
        plane.run();
        System.out.println("------------");
        
        Car car = new Car("Engine number 2");
        car.StartEngine();
        car.run();
        System.out.println("------------");
        
        Flyable[] flyables = {bat,plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.StartEngine();
                p.run();
            }
            f.fly();
        }
        System.out.println("------------");
        
        Runable[] runables = {dog,plane,car};
        for(Runable r: runables){
            r.run();
        }
    }
}
